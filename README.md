# COROUTINES

Here I will explain the basic concepts about multithreading and how concurrent programming can be simplifie using Kotlin and Coroutines.

Coroutines generalize subroutines for non-preemptive multitasking, by allowing execution to be suspended and resumed. You can write asynchronous code as if it were synchronous. **Are an established and reliable concepts, based on low-level scheduling**. There are no threads, are low-level mechanisms that use **thread pools** to shuffle work between threads. Too many threads cna take up a lot of memory, ultimately crashing the program.

Coroutines don't allways create new threads, they can reuse existing ones from thread pools.

**Is a solution to write concurrent and asynchronous code in a clean and sequential style.** 

## Asynchronous programming

The **main thread** is the thread responsible for managing the UI. Every application can only have one main thread in order to avoid deadlock (many threads access the same resources in a different order). The other threads, not responsible for rendering the UI, are called **worker threads** or **background** threads. 

A **blocking call** is essentially a function that only returns when it has completed.

The ability to allow the execution of mulitple threads is called **multithreading**.

Set the techniques used to control their collaoration and synchronization, **concurrency**. Know how the threads communicate is key to understand the full potential of concurrency.
In order to communicate, different threads need to share data. Accessing the same data from multiple threads, mantaining the correct behaviour and good performance is the real challenge of concurrent programming.

Inmutable objects are always thread safe.
The most importante data structures used to safely share data in a thread are **queues and pipelines**.

### Queues
Threads are usually communicate using queues, and they can act on them as **producers** (thread that puts information into the queue / append to the end) or **consumers** (reads and uses them / read from the top). *FIFO*. The action of put data into the queue is called **message**. 
A queue is a container that provides synch in order to allow a thread to consume a message only if it is available. Otherwise it waits if the message is not available.
The queue can be a **blocking queue**, the consumer can block and wait for a new message or just try later.

A good example are fast food lines:

Imagine having three lines at a fast food restaurant. The first line has no customers, so the person working the line is blocked until someone arrives. The second has customers, so the line is slowly getting smaller as the worker serves customers. However, the last line is full of customers, but there’s no one to serve them; this, in turn, blocks the line until help arrives. In this example customers form a queue waiting to consume what the fast food workers are preparing for them. When the food is available, the customer consumes it and leaves the queue. You could also look at the customers as produced work, which the workers need to consume and serve, but the idea stays the same.

### Pipelines
There's a pipe that allows streams of data to flow, and there are listeners. The data is usually a stream of bytes, which listeners parse into something more meaningful.

A good example could be a factory line. If there's too much product (data), te line (pipeline) has stop (blocked) until you process everything (consume some of the data). And alternatively, if there's not enough product, the person processing it sits and waits until something comes up.

### Indentation Hell
When nesting callbacks, or lambdas, you get a large number of braces '{}', each froming a local scope. This creates a structue called **indentation hell** o **callback hell**

### Reactive extensions for background work
Rx incorporates the observer pattern into helpful constructs. Furthermore, there are a large number of operators that extend the behaviour of observable streams, allowing for clean and expressive data processing. **Reactive extensions are cleaner than callbacks when it comes to asynch programming, but they also have a steeper learning curve and has a lot of operators that not allways are useful.** 

